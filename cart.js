<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Audit Report</title>
       
   
    </head>
    <body>
	
	<form id="add-form">
		  <label>Name:<span id="item-name" >Herbal Soap</span></label>
		  <label>Price:<span id="item-price">2.99</span></label>
		  <label> Quantity: <input  id="item-qty" type="number" ></label>
		 
		  <button type="submit">Add item</button>
			
		</form>
	
	
		<form id="add-form1">
		  <label>Name:<span id="item-name1">Rose Soap</span></label>
		  <label>Price:<span id="item-price1">4.99</span></label>
		  <label> Quantity: <input  id="item-qty" type="number" ></label>
		 
		  <button type="submit">Add item</button>
			
		</form>
		
		
	
		
	
	
		<!--<form id="add-form">
		  <label>Name: <input id="item-name" type="text" placeholder="Item Name"> </label>
		  <label> Price: <input  id="item-price" type="number" step="0.01"></label>
		  <button type="submit">Add item</button>
		
		</form>
	    -->
		
		<div id="cart-qty">
		<!--cart qty here -->
		</div>
		
		
		<ul id="item-list">
		   <!--cart items-->
		</ul>
	
	    <div id="cart-total">
		<!--cart Total -->
		</div>
	
	
        <!-- Create <script> block here -->
        <script type="text/javascript">
        <!--
		
		    const itemList = document.getElementById("item-list");
			const cartQty  = document.getElementById('cart-qty');
			const cartTotal =  document.getElementById('cart-total');
			const addForm = document.getElementById('add-form');
			const addForm1 = document.getElementById('add-form1');
			const itemName = document.getElementById('item-name');
			const itemName1 = document.getElementById('item-name1');
			const itemPrice = document.getElementById('item-price');
			const itemPrice1 = document.getElementById('item-price1');
			const itemQty = document.getElementById('item-qty');
			
            const cart = [];   
			console.log(cart);
			//---------------------------------------------------
			//handle change events on update input
			itemList.onchange = function(e) {
			  if (e.target && e.target.classList.contains('update')) {
			  const name = e.target.dataset.name
			    //console.log(e.target)
			  const qty = parseInt(e.target.value)
			  updateCart(name, qty)
			
			
			  }
			
			
			}

			//----------------------------------------
			//handle clicks on list
			itemList.onclick = function(e) {
			//console.log("clicked") //here click on everything in li
			//console.log(e.target)  //show what I clicked button or li
			   if(e.target && e.target.classList.contains('remove')) {
			       const name = e.target.dataset.name   //dwoluje sie do: data-name="${name}"
				   removeItem(name)  //w funkcji remove musze dodac showItems() bo nie bedzie dzialac 
			   } else if (e.target && e.target.classList.contains('add-one')) {
			       const name = e.target.dataset.name   //
				  
				   addItem1(name)   //// here I should define qty
			   }  else if (e.target && e.target.classList.contains('remove-one')) {
			       const name = e.target.dataset.name   //
				   removeItem(name,1)
			   }
			
			
			}
			
			
		
			//----------------------------------------------------------
			// handle add form submit
			addForm.onsubmit = function(e) {
				e.preventDefault()   // use it only when you have a form to prevent from wrap
				console.log(e);
				const name = itemName.innerHTML
				const price = itemPrice.innerHTML
				const qty = itemQty.value
				addItem(name,price,qty)
				 showItems();
			}

			addForm1.onsubmit = function(e) {
				e.preventDefault()   // use it only when you have a form to prevent from wrap
				console.log(e);
				const name = itemName1.innerHTML
				const price = itemPrice1.innerHTML
				const qty = itemQty.value
				addItem(name,price,qty)
				 showItems();
			}

			//--------------------------------------------
			// add item

			
			function addItem(name, price, qty) {
			  for (let i=0; i< cart.length; i+=1) {
			  
			    if(cart[i].name === name) {
				  if(qty < 1) {
				    removeItem(name)
					return
				  }
				  cart[i].qty = qty
				  showItems()
				  return
				}
			  }
			  const item = {name, price, qty};
				cart.push(item);
			}
			
			
	
			function addItem1(name, price, qty) {
			   for (let i=0; i<cart.length; i+=1) {
			
			      if(cart[i].name === name) {
				    cart[i].qty ++
					showItems()
					return  // stop here
				 }
			}			   
			
			    const item = {name, price, qty: 1};
				cart.push(item);
			   
			}
			
	
			//--------------------------------------------
			//show Items
            function showItems() {
			  
				console.log(`You have ${getQty()} items in your cart`);
				//console.log(cart);
				cartQty.innerHTML = `You have ${getQty()} items in your cart`
				
				
				let itemStr = '';
				for (let i=0; i<cart.length; i+=1) {
				   // console.log(`-${cart[i].name} ${cart[i].price} x ${cart[i].qty}`)
					//cosnt name = cart[i].name
					//cosnt price = cart[i].price
					//cosnt qty = cart[i].qty
				
					itemStr += `<li>
          
		  
              ${cart[i].name} ${cart[i].price} each 
              x ${cart[i].qty} = ${(cart[i].qty * cart[i].price).toFixed(2)}
      
       
              <button class="remove" data-name="${cart[i].name}">remove</button>
              <button class="add-one" data-name="${cart[i].name}"> + </button>
              <button class="remove-one" data-name="${cart[i].name}"> - </button>
              <input class="update" data-name="${cart[i].name}" type="number">
         
          </li>`
					
				}
				itemList.innerHTML = itemStr;
				
				
				const total = getTotal();
			    //console.log(`Total in cart: $${total}`);
				cartTotal.innerHTML = `Total in cart: $${total}`
			    
			}
			
			//--------------------------------------------
			// Get Qty
			function getQty() {
			    let qty = 0;
			    for (let i =0; i< cart.length; i +=1) {
				qty += cart[i].qty;
				}
			    return qty;
			
			}

			//--------------------------------------------
			// Get total
			function getTotal() {
			    let total = 0;
				for (let i=0; i<cart.length; i +=1) {
				    total +=cart[i].price * cart[i].qty;
				}
			    return total.toFixed(2);
			}
			
			function removeItem(name, qty=0) {
				for  (let i=0; i<cart.length; i+=1) {
				  if(cart[i].name === name) {
				     if(qty >0) {
					   cart[i].qty -= qty
					 }
					 if(cart[i].qty < 1 || qty === 0){
					   cart.splice(i,1)
					 }   
					 showItems()
					return 
				  } 
				}
			}
			
			
			/*
			function removeItem(name) {
				for  (let i=0; i<cart.length; i+=1) {
				  if(cart[i].name === name) {
				     cart[i].qty -=1;
					 if(cart[i].qty ===0) {
					   cart.splice(i,1)
					 }
					return
					 
				  }
				  
				}
			
			}	
            */
			//--------------------------------------------
			
			//----------------------------------------------
			//update cart
			
			function updateCart(name, qty) {
			  for (let i=0; i< cart.length; i+=1) {
			    if(cart[i].name === name) {
				  if(qty < 1) {
				    removeItem(name)
					return
				  }
				  cart[i].qty = qty
				  showItems()
				  return
				}
			  }
			}
			
			
          
		  





        //-->
        </script>

    </body>
</html>
